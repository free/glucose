#include <spdlog/spdlog.h>

namespace GLucose
{
    
    template<typename ... Ts>
    inline void Log(const char* format, const Ts&... args)
    {
        spdlog::info(format, args...);
    }

    template<typename ... Ts>
    inline void LogFatal(const char* format, const Ts&... args) {
        spdlog::critical(format, args...);
    }

    template<typename ... Ts>
    inline void LogWarn(const char* format, const Ts&... args) {
        spdlog::warn(format, args...);
    }

}