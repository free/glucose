#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace GLucose
{

    class BaseWindow
    {
    public:
        BaseWindow(const char* title, int width, int height);

        ~BaseWindow();

    protected:
        GLFWwindow* m_Window;
    };

}