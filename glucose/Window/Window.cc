#include <glad/glad.h>
#include "Window.hh"

namespace GLucose
{

    Window::Window(const char* title, int width, int height)
        : BaseWindow(title, width, height)
    {
    }

    void Window::PollEvents()
    {
        glfwSwapBuffers(this->m_Window);
        glfwPollEvents();
    }

    bool Window::ShouldClose()
    {
        return glfwWindowShouldClose(this->m_Window);
    }

}