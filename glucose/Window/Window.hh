#pragma once
#include <glad/glad.h>
#include "Window/BaseWindow.hh"

namespace GLucose
{

    class Window : public BaseWindow
    {
    public:
        Window(const char* title, int width, int height);

        // Poll pending events and swap window buffers
        // Should only be called when the other (non-drawn) window buffer is ready
        void PollEvents();

        bool ShouldClose();

    protected:
    private:
    };

}