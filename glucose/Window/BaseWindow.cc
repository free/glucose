#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "BaseWindow.hh"
#include "Logging/Log.hh"

namespace GLucose 
{
    
    BaseWindow::BaseWindow(const char* title, int width, int height)
    {
        GLucose::Log("Initializing window...");

        if (!glfwInit())
        {
            const char* desc;
            glfwGetError(&desc);

            GLucose::LogFatal("Failed to initialize GLFW: {}", desc);
            exit(-1);
        }

        this->m_Window = glfwCreateWindow(
            width,
            height,
            title,
            nullptr,
            nullptr
        );

        if (!this->m_Window) {
            const char* desc;
            glfwGetError(&desc);

            GLucose::LogFatal("Failed to initialize window: {}", desc);
            glfwTerminate();
            exit(-1);
        }

        glfwMakeContextCurrent(this->m_Window);

        if (!gladLoadGL())
        {
            GLucose::LogFatal("Failed to initialize GL bindings with GLAD!");
            exit(-1);
        }
        
        GLucose::Log("Initialized, OpenGL: {}.{}.", GLVersion.major, GLVersion.minor);
    }

    BaseWindow::~BaseWindow()
    {
        glfwTerminate();
    }

}