#include "Window/Window.hh"
#include <thread>
#include <chrono>

#include <memory>

int main() {
    auto window = std::make_shared<GLucose::Window>("hello", 1024, 720);
    
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    while (!window->ShouldClose())
    {
        glClear(GL_COLOR_BUFFER_BIT);
        window->PollEvents();
    }

    return 0;
}