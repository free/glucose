cmake_minimum_required(VERSION 3.15 FATAL_ERROR)
project(GLucose VERSION 1.0 DESCRIPTION "A modern Open GL SDK")

option(GLUCOSE_BUILD_EXAMPLES ON)

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

add_subdirectory(dependencies)
add_subdirectory(glucose)

if(GLUCOSE_BUILD_EXAMPLES)
    add_subdirectory(examples/barebones)
endif()