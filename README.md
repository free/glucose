# glucose
A modern Open GL SDK

## Dependencies
- [GLFW](https://github.com/glfw/glfw)
- [GLAD](https://github.com/Dav1dde/glad) | [permalink](http://glad.dav1d.de/#profile=core&specification=gl&api=gl%3D3.3&api=gles1%3Dnone&api=gles2%3Dnone&api=glsc2%3Dnone&language=c&loader=on)
- [spdlog](https://github.com/gabime/spdlog)

## Cloning the repo
```shell
$ git clone --recursive https://gitea.com/free/glucose
```

## Using the library
To use the library via CMake, simply clone this repo into same folder as your own project. Add these lines to your CMake project:
```cmake
# This is required, so CMake can figure out what is glucose
add_subdirectory(deps/glucose)

# Then link it with your project
target_link_libraries(${PROJECT_NAME} GLucose)
```

## License
GLucose is licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.

[tl;dr](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)) you can do almost anything with exception of holding liable and using trademark.